mu = 0 - harmonic oscillator
1, 0, 2pi

mu = 4

x0_init = 1
y0_init = -0.68
T_init = 10.200


mu = 5

x0 = 1.0
y0 = -0.63108379346996979
T = 11.607027555167575

# sol for mu = 6
x0_init = 1.0
y0_init = -0.59025546
T_init = 13.05468271


# sol for mu = 6.5
x0_init = 1.0
y0_init = -0.57328626183024323
T_init = 13.789403611556299

# sol for mu=6.9
x0_init = 1.0
y0_init = -0.56098413207836539
T_init = 14.381579258409966

# sol for mu=7
x0_init = 1.0
y0_init = -0.55806575459787033
T_init = 14.530094397873762

# sol for mu=7.5
x0_init = 1.0
y0_init = -0.54430424259010501
T_init = 15.275755610227517

# sol for mu=8
#x0_init = 1.0
#y0_init = -0.53177512627743617
#T_init = 16.025702001224069
