import numpy as np
from rk import rk2d
from rk import rkmIter2d
import scipy.optimize as opt
from f_vanderpol import fx_vanderpol as fx
from f_vanderpol import fy_vanderpol as fy
from myexceptions import *
from settings import *

# params = [mu]
def vanderpol_solve(ic, T, params, dt):
  if T < 0:
    #T = -T
    dt = -dt

  #return vanderpol_solve_leapfrog(ic, T, params, dt)
  #return vanderpol_solve_euler_expl(ic, T, params, dt)
  #return vanderpol_solve_euler_semi(ic, T, params, dt)  # good one before rkm
  #return vanderpol_solve_rk(ic, T, params, dt)
  return vanderpol_solve_rkm(ic, T, params, dt)



# forward Euler. not good enough
def vanderpol_solve_euler_expl(ic, T, params, dt):
  [x0, y0] = ic
  x = x0
  y = y0
  t = 0
  while t < T:
    x0 = x
    y0 = y
    x = x0 + dt * fx(x0, y0, params[0])
    y = y0 + dt * fy(x0, y0, params[0])   
    t = t + dt
  return [x,y]


def vanderpol_solve_rk(ic, T, params, dt):
  [x0, y0] = ic
  x = x0
  y = y0
  t = 0

  def f(t, x, y):
    return fx(x, y, params[0])

  def g(t, x, y):
    return fy(x, y, params[0])

  while t < T:
    (xn,yn) = rk2d(t, x, y, dt, f, g)
    x = xn
    y = yn
    t = t + dt
  return [x,y]


# inlined fx and fy to avoid overhead
def vanderpol_solve_euler_semi(ic, T, params, dt):
  [x0, y0] = ic
  x = x0
  y = y0
  t = 0
  while t < T:
    y = y + dt * (params[0] * (1 - x**2) * y - x)
    x = x + dt * y
    t = t + dt
  return [x,y]
 

def vanderpol_solve_leapfrog(ic, T, params, dt):
  [x0, y0] = ic
  x = x0
  y = y0
  t = 0

  def F(y, y0, x0, params, dt):
    return -y + y0 + 1/2 * (fy(x0, y0, params[0]) + fy(x, y, params[0])) * dt

  while t < T:
    x0 = x
    y0 = y
    x = x0 + y0 * dt + 1/2 * fy(x0, y0, params[0]) * dt**2
    y = opt.newton(F, y0, args = (y0, x0, params, dt[0]))
    t = t + dt

  return [x,y]


# implicit midpoint - symplectic
def vanderpol_solve_midpoint(ic, T, params, dt):
  [x0, y0] = ic
  x = x0
  y = y0
  t = 0

  def F(y, x0, y0):
    return y - y0 - dt * fy(1/2 * (x0 + x0 + dt * fx(1/2*(x0 + x), 1/2*(y0 + y),params[0])), 1/2 * (y0 + y), params[0])
 
  while t < T:
    x0 = x
    y0 = y

    y = opt.newton(F, y0, args = (x0, y0))
    x = x0 + dt * fx(1/2*(x0 + x), 1/2*(y0 + y), params[0])
    t = t + dt

  
  return [x,y]

rkm_epsilon = 1e-11
timestep_tol = 1e-8
def vanderpol_solve_rkm(ic, T, params, dt):
  epsilon = rkm_epsilon
  [a0, b0] = ic
  [mu] = params
  a = a0
  b = b0
  t = 0

  def f(a,b):
    return fx(a,b,mu)
  
  def g(a,b):
    return fy(a,b,mu)

  # We need to prevent the situation where it only doubles and halves the dt 
  # infinitely many times and does not change t.
  # This is what idx1 and idx2 do
  idx1 = 0
  idx2 = 0
  while abs(t) < abs(T):
    (an1,bn1,an2,bn2) = rkmIter2d(a,b,dt,f,g)
    R = 0.2 * np.linalg.norm([an1-an2, bn1-bn2])

    if idx1 > 10 and abs(idx1 - idx2) <= 1:
      R = epsilon

    if R > epsilon:
      dt = dt / 2
      idx1 +=1
    elif R > epsilon / 64:
      # In case the step size is too large, reduce it so that we calculate the 
      # the state at T
      if (abs(t + dt) > abs(T)):
        dt = T - t
        (an1,bn1,an2,bn2) = rkmIter2d(a,b,dt,f,g)
      
      a = an2
      b = bn2
      t += dt
      idx1 = 0
      idx2 = 0
    else: # if R leq epsilon/64
      dt = dt * 2
      idx2 += 1

    if abs(dt) < timestep_tol:
      raise TimeStepTooSmallException

  return [a,b]

# This is the same as vadernpol_solve_rkm except 
# that it returns all the values in vectors,
# rather than only the value at T.
# Needed for plotting
def vanderpol_solve_rkm_vector(ic, T, params, dt):
  epsilon = rkm_epsilon
  [a0, b0] = ic
  [mu] = params
  a = [a0]
  b = [b0]
  t = [0]

  def f(a,b):
    return fx(a,b,mu)
  
  def g(a,b):
    return fy(a,b,mu)
  
  # We need to prevent the situation where it only doubles and halves the dt 
  # infinitely many times and does not change t.
  # This is what idx1 and idx2 do
  idx1 = 0
  idx2 = 0
  while abs(t[-1]) < abs(T):
    #print()
    if verbose:
      print("dt = " + repr(dt))
      print(" t  = " + repr(t[-1]))
    #print(" idx= " + repr(idx))
    (an1,bn1,an2,bn2) = rkmIter2d(a[-1],b[-1],dt,f,g)
    R = 0.2 * np.linalg.norm([an1-an2, bn1-bn2])
    #print("R  = " + repr(R))
    #print("eps= " + repr(epsilon))

    if idx1 > 10 and abs(idx1 - idx2) <= 1:
      R = epsilon

    if R > epsilon:
      dt = dt / 2
      idx1 +=1
    if R <= epsilon and R > epsilon / 64:
      if (abs(t[-1] + dt) > abs(T)):
        dt = T - t[-1]
        (an1,bn1,an2,bn2) = rkmIter2d(a[-1],b[-1],dt,f,g)
 
      a.append(an2)
      b.append(bn2)
      t.append(t[-1] + dt)
      idx1 = 0
      idx2 = 0
    if R <= epsilon/64:
      dt = dt * 2
      idx2 += 1
  
  return (t,a,b)

