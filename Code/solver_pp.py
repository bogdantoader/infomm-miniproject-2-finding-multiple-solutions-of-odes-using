import numpy as np
from rk import rkmIter2d
from f_pp import fx_pp as fx
from f_pp import fy_pp as fy
from myexceptions import *
from settings import *



def pp_solve(ic, T, params, dt):
  if T < 0:
    dt = -dt

  return pp_solve_rkm(ic, T, params, dt)


rkm_epsilon = 1e-10
timestep_tol = 1e-8
def pp_solve_rkm(ic, T, params, dt):
  epsilon = rkm_epsilon
  [a0, b0] = ic
  [p1, p2, p3, p4] = params
  a = a0
  b = b0
  t = 0

  def f(a,b):
    return fx(a,b,p1, p2, p3, p4)
  
  def g(a,b):
    return fy(a,b,p1, p2, p3, p4)

  # We need to prevent the situation where it only doubles and halves the dt 
  # infinitely many times and does not change t.
  # This is what idx1 and idx2 do
  idx1 = 0
  idx2 = 0
  while abs(t) < abs(T):
    (an1,bn1,an2,bn2) = rkmIter2d(a,b,dt,f,g)
    R = 0.2 * np.linalg.norm([an1-an2, bn1-bn2])

    if idx1 > 10 and abs(idx1 - idx2) <= 30:
      R = epsilon

    if R > epsilon:
      dt = dt / 2
      idx1 +=1
    elif R > epsilon / 64:
      # In case the step size is too large, reduce it so that we calculate the 
      # the state at T
      if (abs(t + dt) > abs(T)):
        dt = T - t
        (an1,bn1,an2,bn2) = rkmIter2d(a,b,dt,f,g)
      
      a = an2
      b = bn2
      t += dt
      idx1 = 0
      idx2 = 0
    else: # if R leq epsilon/64
      dt = dt * 2
      idx2 += 1

    if abs(dt) < timestep_tol:
      raise TimeStepTooSmallException

  return [a,b]

# This is the same as vadernpol_solve_rkm except 
# that it returns all the values in vectors,
# rather than only the value at T.
# Needed for plotting
def pp_solve_rkm_vector(ic, T, params, dt):
  epsilon = rkm_epsilon
  [a0, b0] = ic
  [p1, p2, p3, p4] = params
  a = [a0]
  b = [b0]
  t = [0]

  def f(a,b):
    return fx(a,b,p1, p2, p3, p4)
  
  def g(a,b):
    return fy(a,b,p1, p2, p3, p4)
  
  # We need to prevent the situation where it only doubles and halves the dt 
  # infinitely many times and does not change t.
  # This is what idx1 and idx2 do
  idx1 = 0
  idx2 = 0
  while abs(t[-1]) < abs(T):
    #print()
    if verbose:
      print("dt = " + repr(dt))
      print(" t  = " + repr(t[-1]))
    #print(" idx= " + repr(idx))
    (an1,bn1,an2,bn2) = rkmIter2d(a[-1],b[-1],dt,f,g)
    R = 0.2 * np.linalg.norm([an1-an2, bn1-bn2])
    #print("R  = " + repr(R))
    #print("eps= " + repr(epsilon))

    if idx1 > 10 and abs(idx1 - idx2) <= 30:
      R = epsilon
    if R > epsilon:
      dt = dt / 2
      idx1 +=1
    if R <= epsilon and R > epsilon / 64:
      if (abs(t[-1] + dt) > abs(T)):
        dt = T - t[-1]
        (an1,bn1,an2,bn2) = rkmIter2d(a[-1],b[-1],dt,f,g)
 
      a.append(an2)
      b.append(bn2)
      t.append(t[-1] + dt)
      idx1 = 0
      idx2 = 0
    if R <= epsilon/64:
      dt = dt * 2
      idx2 += 1
  return (t,a,b)


