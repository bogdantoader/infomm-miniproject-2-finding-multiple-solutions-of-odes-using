import numpy as np

from jacobian import calculate_jacobian
from deflation import deflation
from augmented_F import get_F_lorenz
from solver_lorenz import lorenz_solve
from continuation import continuation
from create_csv import create_csv

np.seterr(all='raise')

# Lorenz system parameters
beta = 8.0/3
sigma = 10.0

# general parameters
iters = 50
tol = 1e-10
dt = 0.00001
eps = 0.00025


# continuation parameters
#[rho,sigma,beta] = params

# ic for rho = 12.0
#ic = [np.array([4.9123745971183824e-15,4.9123745971184076e-15,-4.7252010643869614e-17,1.1264038644993193]),
#np.array([-5.4160256030861778,-5.4160256030861778,10.999999999996195,11.112671054271173]),
#np.array([5.4160256092815837,5.4160256092815837,11.000000014431532,0.0010313183288562426])]

# ic for rho = 31.0
ic = [np.array([1.3598533607909821e-15,1.3598533607909821e-15,-6.846290676172516e-24,0.98683280091787462]),
np.array([-8.9442719099952654,-8.9442719099952654,29.99999999999503,10.650756120389293]),
np.array([8.9442719100045931,8.9442719100045931,30.000000000021654,0.0010312968427343011])]

#ic = [np.array([0.1,0.1,0.1,1])]
r0 = [31.0, sigma, beta]
dr = 0.05
rF = [40.0, sigma, beta]



# functions

def jacobian(F, x):
  return calculate_jacobian(F, x, eps)


# and run
sols = continuation(r0, rF, dr, ic, lorenz_solve, get_F_lorenz, jacobian, tol, eps, dt, iters)

output = create_csv(sols)
print(output)
f = open('lorenz_sols_31-101.txt', 'w')
f.write(output)
f.close()
