import numpy as np
#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import csv
from solver_mc import mc_solve_rkm_vector


filename = "../Solutions/solutions_q25.txt"
dt = 0.00001

plt.rc('text', usetex=True)
plt.figure(1)

# store the raw data
raw_data = ""

with open(filename) as csvfile:
  rdr = csv.reader(csvfile)
  for row in rdr:
    r = float(row[0])
    
    x = np.array([ float(row[1]),float(row[2]),float(row[3]),
                float(row[4]),float(row[5]),float(row[6])])
    #print(repr(r) + ", " + repr(x))

    params = (r, 1, 0.4, 2.5, 2.0)
    #params = (r, 10, 0.4, 5.0, 2.0)
    (t,a,b,c,d,e) = mc_solve_rkm_vector(x[0:5],x[5],params,dt)
    if x[0] > 0:
      p = max(a)
    else:
      p = min(a)

    
    if x[-1] > 1 and abs(x[0]) > 1e-8:
      plt.plot(r, p, 'r.')
      raw_data += (repr(r) + "," + repr(p) + "," + repr(1) + "\n")
    else:
      plt.plot(r, p, 'b.')
      raw_data += (repr(r) + "," + repr(p) + "," + repr(0) + "\n")


# print the raw data for the figure to a file
f = open('solutions_q25_raw.txt', 'w')
f.write(raw_data)
f.close()


plt.xlabel('\(r\)', fontsize=30)
plt.ylabel('\(a_{max}\)', fontsize=30)
plt.xlim((2,6.5))
#plt.savefig('bifurcation_q5')
plt.show()



