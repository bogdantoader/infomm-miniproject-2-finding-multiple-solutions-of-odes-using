

# create a csv string from sols
# sols is a dictionary: the key is the value parameter for each contiuation iteration
# and the value is a list of solutions (numpy arrays)
def create_csv(sols):
  output = ""
  for k in sols:
    for s in sols[k]:
      output += (repr(k) + ",")
      for n in s:
        output += (repr(n) + ",")
      output += "\n"

  return output

