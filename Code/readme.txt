

To use the code with a new system:

1. Implement a solver for a specific system. I recommend 
  using Runge-Kutta-Merson, already implemented for 2D-5D in rk.py
  If RKM is chosen, also implement a *_vector version to be used for plotting
  See, for example, solver_vanderpol.py, solver_mc.py, solver_lorenz.py
2. Add a get_F function in augmented_F.py function.
  That's where you specify the phase condition.
3. Write a run_x.py script that puts everything together. 
  See run_mc.py, run_vanderpol.py, run_lorenz.py
4. Write a plot_xx.py to plot the solution
  See plot_mc.py, plot_vanderpol.py, plot_lorenz.py
5. Write a plot_diagram_xx.py to plot the bifurcation diagram
  See plot_diagram_mc.py, plot_diagram_vanderpol.py
