import numpy as np


def calculate_jacobian(F, x, eps):
  n = len(x)

  J = np.zeros([n, n])

  for i in range(n):
    xp = np.copy(x)
    xp[i] = x[i] + eps
    xm = np.copy(x)
    xm[i] = x[i] - eps
    coli = (F(xp) - F(xm)) / (2*eps)
    J[:,i] = coli

  return J


