import numpy as np
import matplotlib.pyplot as plt
from solver_pp import *
from augmented_F import get_F_pp

dt = 0.00001





p1 = 0.671
p2 = 3
p4 = 3
p3 = 5

x0 = np.array([0.3333333357338743,0.36720658977156645,10.368492658877502])

T = x0[-1]

if T < 0:
  dt = -dt

(t,x,y) = pp_solve_rkm_vector(x0[0:2], T, [p1,p2,p3,p4], dt)
print(len(t))
print("x(T) = " + repr([x[-1], y[-1]]))


plt.figure(1)
plt.plot(x, y, linewidth=2)
#plt.title('phase plane')
plt.xlabel('x', fontsize=20)
plt.ylabel('y', fontsize=20)


plt.figure(2)
plt.plot(t, x, linewidth=2)
plt.xlabel('t', fontsize=20)
plt.ylabel('x', fontsize=20)

plt.figure(3)
plt.plot(t, y, linewidth=2)
plt.xlabel('t', fontsize=20)
plt.ylabel('y', fontsize=20)


plt.show()



