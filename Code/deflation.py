import numpy as np
from newtons_method import newtons_method
from myexceptions import * 
from settings import *
from colours import *



def deflation(F, x_init, jacobian, tol, max_iters=30,p=1, sigma=2):
  printGreen("Starting point: " + repr(x_init))
  printGreen("Solution 1")
  try:
    sol = newtons_method(F, x_init, jacobian, tol, max_iters) 
  except (MaxIterationsException, ResidualTooLargeException,TimeStepTooSmallException,FloatingPointError, np.linalg.linalg.LinAlgError) as e:
    printRed("No solution found, " + repr(e))
    return []
  

  def createG(F, sol):
    def G(u):
      n = len(u)
      M = 1.0 / (np.linalg.norm(u[0:n-1] - sol[0:n-1])**p) + sigma
      #M = 1.0 / (np.linalg.norm(u - sol)**p) + sigma
      r = F(u)
      #return [M*r[0], M*r[1], M*r[2]]
      return M * r
    return G
  
  i = 0
  funcs = [F]
  sols = [sol]
  while True:
  #while False:
    funcs.append(createG(funcs[i], sols[i]))
    i = i + 1
  
    printGreen("Solution " + repr(i + 1))
    try:
      sol = newtons_method(funcs[i], x_init, jacobian, tol, max_iters)
      sols.append(sol)
    except MaxIterationsException:
      printRed("REACHED MAXIMUM ITERATIONS!!!")
      break
    except ResidualTooLargeException:
      printRed("RESIDUAL TOO LARGE")
      break
    except TimeStepTooSmallException:
      printRed("TIMESTEP TOO SMALL. Abandoning iteration.")
      break
    except np.linalg.linalg.LinAlgError as e:
      printRed("LIN ALG ERROR")
      printRed(repr(e))
      break
    except FloatingPointError as e:
      printRed("FLOATING POINT ERROR")
      printRed(repr(e))
      break
  
  return sols



