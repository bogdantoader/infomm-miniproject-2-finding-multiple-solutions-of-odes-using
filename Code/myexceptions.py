
    
class MaxIterationsException(Exception):
  pass

class ResidualTooLargeException(Exception):
  pass
        
class TimeStepTooSmallException(Exception):
  pass

