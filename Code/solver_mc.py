import numpy as np
from f_mc import fa_mc as fa
from f_mc import fb_mc as fb
from f_mc import fc_mc as fc
from f_mc import fd_mc as fd
from f_mc import fe_mc as fe
from rk import rk5d
from rk import rkmIter5d
from rk import rkmIter6d
from myexceptions import *
from settings import *

def mc_solve(ic, T, params, dt):
  if T < 0:
    #T = -T
    dt = -dt

  #return mc_solve_rk(ic, T, params, dt)
  return mc_solve_rkm(ic, T, params, dt)

def mc_solve_rk(ic, T, params, dt):
  [a0, b0, c0, d0, e0] = ic
  [r, sigma, zeta, q, pi] = params
  a = a0
  b = b0
  c = c0
  d = d0
  e = e0
  t = 0

  def f(a,b,c,d,e):
    return fa(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def g(a,b,c,d,e):
    return fb(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def h(a,b,c,d,e):
    return fc(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def i(a,b,c,d,e):
    return fd(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def j(a,b,c,d,e):
    return fe(a,b,c,d,e,sigma,r,zeta,q,pi)


  while t < T:
    (an,bn,cn,dn,en) = rk5d(a,b,c,d,e,dt,f,g,h,i,j)
    a = an
    b = bn
    c = cn
    d = dn
    e = en
    t += dt

  return [a,b,c,d,e]

#rkm_epsilon = 0.0000000000001
#rkm_epsilon = 0.0000000001 # <-- good results here
rkm_epsilon = 1e-11
#rkm_epsilon = 1e-16
timestep_tol = 1e-7
def mc_solve_rkm(ic, T, params, dt):
  epsilon = rkm_epsilon
  [a0, b0, c0, d0, e0] = ic
  [r, sigma, zeta, q, pi] = params
  a = a0
  b = b0
  c = c0
  d = d0
  e = e0
  t = 0

  def f(a,b,c,d,e):
    return fa(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def g(a,b,c,d,e):
    return fb(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def h(a,b,c,d,e):
    return fc(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def i(a,b,c,d,e):
    return fd(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def j(a,b,c,d,e):
    return fe(a,b,c,d,e,sigma,r,zeta,q,pi)

  # We need to prevent the situation where it only doubles and halves the dt 
  # infinitely many times and does not change t.
  # This is what idx1 and idx2 do
  idx1 = 0
  idx2 = 0
  while abs(t) < abs(T):
    (an1,bn1,cn1,dn1,en1,an2,bn2,cn2,dn2,en2) = rkmIter5d(a,b,c,d,e,dt,f,g,h,i,j)
    R = 0.2 * np.linalg.norm([an1-an2, bn1-bn2, cn1-cn2, dn1-dn2, en1-en2])

    if idx1 > 10 and abs(idx1 - idx2) <= 1:
      R = epsilon

    if R > epsilon:
      dt = dt / 2
      idx1 +=1
    elif R > epsilon / 64:
      # In case the step size is too large, reduce it so that we calculate the 
      # the state at T
      if (abs(t + dt) > abs(T)):
        dt = T - t
        (an1,bn1,cn1,dn1,en1,an2,bn2,cn2,dn2,en2) = rkmIter5d(a,b,c,d,e,dt,f,g,h,i,j)
      
      a = an2
      b = bn2
      c = cn2
      d = dn2
      e = en2
      t += dt
      idx1 = 0
      idx2 = 0
    else: # if R leq epsilon/64
      dt = dt * 2
      idx2 += 1

    if abs(dt) < timestep_tol:
      raise TimeStepTooSmallException

  return [a,b,c,d,e]

# This is the same as mc_solve_rkm except that it returns all the values in vectors,
# rather than only the value at T.
# Needed for plotting
def mc_solve_rkm_vector(ic, T, params, dt):
  epsilon = rkm_epsilon
  [a0, b0, c0, d0, e0] = ic
  [r, sigma, zeta, q, pi] = params
  a = [a0]
  b = [b0]
  c = [c0]
  d = [d0]
  e = [e0]
  t = [0]

  def f(a,b,c,d,e):
    return fa(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def g(a,b,c,d,e):
    return fb(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def h(a,b,c,d,e):
    return fc(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def i(a,b,c,d,e):
    return fd(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def j(a,b,c,d,e):
    return fe(a,b,c,d,e,sigma,r,zeta,q,pi)

  # We need to prevent the situation where it only doubles and halves the dt 
  # infinitely many times and does not change t.
  # This is what idx1 and idx2 do
  idx1 = 0
  idx2 = 0
  while abs(t[-1]) < abs(T):
    #print()
    if verbose:
      print("dt = " + repr(dt))
      print(" t  = " + repr(t[-1]))
    #print(" idx= " + repr(idx))
    (an1,bn1,cn1,dn1,en1,an2,bn2,cn2,dn2,en2) = rkmIter5d(a[-1],b[-1],c[-1],d[-1],e[-1],dt,f,g,h,i,j)
    R = 0.2 * np.linalg.norm([an1-an2, bn1-bn2, cn1-cn2, dn1-dn2, en1-en2])
    #print("R  = " + repr(R))
    #print("eps= " + repr(epsilon))

    if idx1 > 10 and abs(idx1 - idx2) <= 1:
      R = epsilon

    if R > epsilon:
      dt = dt / 2
      idx1 +=1
    if R <= epsilon and R > epsilon / 64:
      if (abs(t[-1] + dt) > abs(T)):
        dt = T - t[-1]
        (an1,bn1,cn1,dn1,en1,an2,bn2,cn2,dn2,en2) = rkmIter5d(a[-1],b[-1],c[-1],d[-1],e[-1],dt,f,g,h,i,j)
 
      a.append(an2)
      b.append(bn2)
      c.append(cn2)
      d.append(dn2)
      e.append(en2)
      t.append(t[-1] + dt)
      idx1 = 0
      idx2 = 0
    if R <= epsilon/64:
      dt = dt * 2
      idx2 += 1
  
  return (t,a,b,c,d,e)

# Solver for 6d system - to illustrate Hopf bifurcation
# adding extra equation for r, \dot{r} = C, where C in (0,1) 
def mc_solve_rkm_vector_6d(ic, T, params, dt):
  epsilon = rkm_epsilon
  [a0, b0, c0, d0, e0, r0] = ic
  [sigma, zeta, q, pi, C] = params
  a = [a0]
  b = [b0]
  c = [c0]
  d = [d0]
  e = [e0]
  r = [r0]
  t = [0]

  def f(a,b,c,d,e,r):
    return fa(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def g(a,b,c,d,e,r):
    return fb(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def h(a,b,c,d,e,r):
    return fc(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def i(a,b,c,d,e,r):
    return fd(a,b,c,d,e,sigma,r,zeta,q,pi)
  
  def j(a,b,c,d,e,r):
    return fe(a,b,c,d,e,sigma,r,zeta,q,pi)

  #  for first one
  #def k(a,b,c,d,e,r):
  #  if r < 5.12:
  #    return C
  #  else:
  #    return 0

  def k(a,b,c,d,e,r):
    return C


  # We need to prevent the situation where it only doubles and halves the dt 
  # infinitely many times and does not change t.
  # This is what idx1 and idx2 do
  idx1 = 0
  idx2 = 0
  while abs(t[-1]) < abs(T):
    if verbose:
      print("dt = " + repr(dt))
      print(" t  = " + repr(t[-1]))
    (an1,bn1,cn1,dn1,en1,rn1,an2,bn2,cn2,dn2,en2,rn2) = rkmIter6d(a[-1],b[-1],c[-1],d[-1],e[-1],r[-1],dt,f,g,h,i,j,k)
    R = 0.2 * np.linalg.norm([an1-an2, bn1-bn2, cn1-cn2, dn1-dn2, en1-en2])
    if idx1 > 10 and abs(idx1 - idx2) <= 1:
      R = epsilon

    if R > epsilon:
      dt = dt / 2
      idx1 +=1
    if R <= epsilon and R > epsilon / 64:
      if (abs(t[-1] + dt) > abs(T)):
        dt = T - t[-1]
        (an1,bn1,cn1,dn1,en1,rn1,an2,bn2,cn2,dn2,en2,rn2) = rkmIter6d(a[-1],b[-1],c[-1],d[-1],e[-1],r[-1],dt,f,g,h,i,j,k)
 
      a.append(an2)
      b.append(bn2)
      c.append(cn2)
      d.append(dn2)
      e.append(en2)
      r.append(rn2)
      t.append(t[-1] + dt)
      idx1 = 0
      idx2 = 0
    if R <= epsilon/64:
      dt = dt * 2
      idx2 += 1
  
  return (t,a,b,c,d,e,r)

