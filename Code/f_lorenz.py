

def fx_lorenz(x,y,z,sigma,rho,beta):
  return sigma * (y - x)
    
def fy_lorenz(x,y,z,sigma,rho,beta):
  return x * (rho - z) - y
    
def fz_lorenz(x,y,z,sigma,rho,beta):
  return x*y - beta*z


