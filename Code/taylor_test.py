import numpy as np

from jacobian import calculate_jacobian
from solver_vanderpol import vanderpol_solve
from solver_lorenz import lorenz_solve
from solver_mc import mc_solve
from f_vanderpol import *
from f_lorenz import *
from augmented_F import *

vanderpol = 0
lorenz = 1
magnetoconvection = 0



if vanderpol == 1:
  dt = 0.0001
  jacob_eps = 0.0001
  mu = 4
  
  u = np.array([1.0,1.0,3.0])
  v = np.array([2.0,-1.0,4.0])
  F = get_F_vanderpol(vanderpol_solve, [mu], dt)

if lorenz == 1:
  dt = 0.00001
  jacob_eps = 0.01
  sigma = 35 
  rho = 16
  beta = 4

  u = np.array([1.0,1.0,1.0,1.0])
  v = np.array([2.0,-1.0,4.0,3.0])
  F = get_F_lorenz(lorenz_solve, [rho, sigma,beta], dt)

if magnetoconvection == 1:
  dt = 0.00001
  jacob_eps = 0.00001
  (sigma, r, zeta, q, pi) = (1.0, 3.5, 0.4, 2.5, 3.0)
  u = np.array([1.0, 1.0, 1.0, 1.0, 1.0, 1.0])
  v = np.array([4.0, 2.0, 6.0, 3.0, 2.0, 5.0])
  F = get_F_mc(mc_solve, [sigma, r, zeta, q, pi], dt)
  
eps = 0.001
i = 1
while i < 10:
  uv = u + eps * v    
  Fu = F(u)
  Fuv = F(uv)
  Fprime = calculate_jacobian(F, u, jacob_eps)
  
  oe = np.linalg.norm(np.asarray(Fuv) - np.asarray(Fu), 2)
  oesq = np.linalg.norm(np.asarray(Fuv) - np.asarray(Fu)  - eps * np.dot(Fprime, v), 2)
  di = np.linalg.norm(np.dot(Fprime,v),2)
  print("eps = " + repr(eps))
  print("  " + repr(oe))
  print("  " + repr(oesq))
  
  eps = eps / 2
  i = i + 1





