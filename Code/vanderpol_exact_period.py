import numpy as np

#Dear Bogdan,
#
#	My notes from E.J.Hinch (who has since published a book on asymptotic methods) give the period of van der Pol as 
#
#T  \sim \mu(3-2 \ln 2) + 3 \mu^{-1/3} s_0
#
#Where s_0 \approx 2.34.
#
#Wayne



#for mu in np.linspace(0.2, 4.4, 22):
def vanderpol_exact_period(mu):  
  s0 = 2.34
  T = mu * (3 - 2* np.log(2)) + 3 * mu**(-1.0/3)*s0
  return T
