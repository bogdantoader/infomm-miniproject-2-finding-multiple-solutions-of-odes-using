

# one iteration of the 4th order Runge-Kutta method for 2D systems

def rk2d(t, x, y, h, f, g):
  k0 = h * f(t,x,y)
  l0 = h * g(t,x,y)

  k1 = h * f(t + 1/2*h, x + 1/2*k0, y + 1/2*l0)
  l1 = h * g(t + 1/2*h, x + 1/2*k0, y + 1/2*l0)

  k2 = h * f(t + 1/2*h, x + 1/2*k1, y + 1/2*l1)
  l2 = h * g(t + 1/2*h, x + 1/2*k1, y + 1/2*l1)

  k3 = h * f(t + h, x + k2, y + l2)
  l3 = h * g(t + h, x + k2, y + l2)

  x = x + 1/6 * (k0 + 2*k1 + 2*k2 + k3)
  y = y + 1/6 * (l0 + 2*l1 + 2*l2 + l3)

  return (x,y)


def rk3d(t, x, y, z, dt, f, g, h):
  k0 = dt * f(t,x,y,z)
  l0 = dt * g(t,x,y,z)
  m0 = dt * h(t,x,y,z)

  k1 = dt * f(t + 1/2*dt, x + 1/2*k0, y + 1/2*l0, z + 1/2*m0)
  l1 = dt * g(t + 1/2*dt, x + 1/2*k0, y + 1/2*l0, z + 1/2*m0)
  m1 = dt * h(t + 1/2*dt, x + 1/2*k0, y + 1/2*l0, z + 1/2*m0)

  k2 = dt * f(t + 1/2*dt, x + 1/2*k1, y + 1/2*l1, z + 1/2*m1)
  l2 = dt * g(t + 1/2*dt, x + 1/2*k1, y + 1/2*l1, z + 1/2*m1)
  m2 = dt * h(t + 1/2*dt, x + 1/2*k1, y + 1/2*l1, z + 1/2*m1)

  k3 = dt * f(t + dt, x + k2, y + l2, z + m2)
  l3 = dt * g(t + dt, x + k2, y + l2, z + m2)
  m3 = dt * h(t + dt, x + k2, y + l2, z + m2)

  x = x + 1/6 * (k0 + 2*k1 + 2*k2 + k3)
  y = y + 1/6 * (l0 + 2*l1 + 2*l2 + l3)
  z = z + 1/6 * (m0 + 2*m1 + 2*m2 + m3)

  return (x,y,z)

def rk5d(x, y, z, u, v, dt, f, g, h, i, j):
  k0 = dt * f(x,y,z,u,v)
  l0 = dt * g(x,y,z,u,v)
  m0 = dt * h(x,y,z,u,v)
  n0 = dt * i(x,y,z,u,v)
  o0 = dt * j(x,y,z,u,v)


  k1 = dt * f(x + 1/2*k0, y + 1/2*l0, z + 1/2*m0, u + 1/2*n0, v + 1/2*o0)
  l1 = dt * g(x + 1/2*k0, y + 1/2*l0, z + 1/2*m0, u + 1/2*n0, v + 1/2*o0)
  m1 = dt * h(x + 1/2*k0, y + 1/2*l0, z + 1/2*m0, u + 1/2*n0, v + 1/2*o0)
  n1 = dt * i(x + 1/2*k0, y + 1/2*l0, z + 1/2*m0, u + 1/2*n0, v + 1/2*o0)
  o1 = dt * j(x + 1/2*k0, y + 1/2*l0, z + 1/2*m0, u + 1/2*n0, v + 1/2*o0)

  k2 = dt * f(x + 1/2*k1, y + 1/2*l1, z + 1/2*m1, u + 1/2*n1, v + 1/2*o1)
  l2 = dt * g(x + 1/2*k1, y + 1/2*l1, z + 1/2*m1, u + 1/2*n1, v + 1/2*o1)
  m2 = dt * h(x + 1/2*k1, y + 1/2*l1, z + 1/2*m1, u + 1/2*n1, v + 1/2*o1)
  n2 = dt * i(x + 1/2*k1, y + 1/2*l1, z + 1/2*m1, u + 1/2*n1, v + 1/2*o1)
  o2 = dt * j(x + 1/2*k1, y + 1/2*l1, z + 1/2*m1, u + 1/2*n1, v + 1/2*o1)

  k3 = dt * f(x + k2, y + l2, z + m2, u + n2, v + o2)
  l3 = dt * g(x + k2, y + l2, z + m2, u + n2, v + o2)
  m3 = dt * h(x + k2, y + l2, z + m2, u + n2, v + o2)
  n3 = dt * i(x + k2, y + l2, z + m2, u + n2, v + o2)
  o3 = dt * j(x + k2, y + l2, z + m2, u + n2, v + o2)

  x = x + 1/6 * (k0 + 2*k1 + 2*k2 + k3)
  y = y + 1/6 * (l0 + 2*l1 + 2*l2 + l3)
  z = z + 1/6 * (m0 + 2*m1 + 2*m2 + m3)
  u = u + 1/6 * (n0 + 2*n1 + 2*n2 + n3)
  v = v + 1/6 * (o0 + 2*o1 + 2*o2 + o3)

  return (x,y,z,u,v)

def rkmIter2d(x,y,dt,f,g):
  k1 = dt * f(x,y)
  l1 = dt * g(x,y)


  k2 = dt * f(x + 1/3*k1, y + 1/3*l1)
  l2 = dt * g(x + 1/3*k1, y + 1/3*l1)

  k3 = dt * f(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2)
  l3 = dt * g(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2)
  
  k4 = dt * f(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3)
  l4 = dt * g(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3) 
  
  k5 = dt * f(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4)
  l5 = dt * g(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4)


  x1 = x + 1/2*k1 - 3/2*k3 + 2*k4
  x2 = x + 1/6*k1 + 2/3*k4 + 1/6*k5
  
  y1 = y + 1/2*l1 - 3/2*l3 + 2*l4
  y2 = y + 1/6*l1 + 2/3*l4 + 1/6*l5

  return (x1,y1,x2,y2)


def rkmIter3d(x,y,z,dt,f,g,h):
  k1 = dt * f(x,y,z)
  l1 = dt * g(x,y,z)
  m1 = dt * h(x,y,z)


  k2 = dt * f(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1)
  l2 = dt * g(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1)
  m2 = dt * h(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1)

  k3 = dt * f(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2)
  l3 = dt * g(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2)
  m3 = dt * h(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2)

  k4 = dt * f(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3)
  l4 = dt * g(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3)
  m4 = dt * h(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3)
  
  k5 = dt * f(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4)
  l5 = dt * g(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4)
  m5 = dt * h(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4)

  x1 = x + 1/2*k1 - 3/2*k3 + 2*k4
  x2 = x + 1/6*k1 + 2/3*k4 + 1/6*k5
  
  y1 = y + 1/2*l1 - 3/2*l3 + 2*l4
  y2 = y + 1/6*l1 + 2/3*l4 + 1/6*l5

  z1 = z + 1/2*m1 - 3/2*m3 + 2*m4
  z2 = z + 1/6*m1 + 2/3*m4 + 1/6*m5
  
  return (x1,y1,z1,x2,y2,z2)


def rkmIter5d(x,y,z,u,v,dt,f,g,h,i,j):
  k1 = dt * f(x,y,z,u,v)
  l1 = dt * g(x,y,z,u,v)
  m1 = dt * h(x,y,z,u,v)
  n1 = dt * i(x,y,z,u,v)
  o1 = dt * j(x,y,z,u,v)


  k2 = dt * f(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1, u + 1/3*n1, v + 1/3*o1)
  l2 = dt * g(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1, u + 1/3*n1, v + 1/3*o1)
  m2 = dt * h(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1, u + 1/3*n1, v + 1/3*o1)
  n2 = dt * i(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1, u + 1/3*n1, v + 1/3*o1)
  o2 = dt * j(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1, u + 1/3*n1, v + 1/3*o1)

  k3 = dt * f(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2, u + 1/6*n1 + 1/6*n2, v + 1/6*o1 + 1/6*o2)
  l3 = dt * g(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2, u + 1/6*n1 + 1/6*n2, v + 1/6*o1 + 1/6*o2)
  m3 = dt * h(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2, u + 1/6*n1 + 1/6*n2, v + 1/6*o1 + 1/6*o2)
  n3 = dt * i(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2, u + 1/6*n1 + 1/6*n2, v + 1/6*o1 + 1/6*o2)
  o3 = dt * j(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2, u + 1/6*n1 + 1/6*n2, v + 1/6*o1 + 1/6*o2)

  k4 = dt * f(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3, u + 1/8*n1 + 3/8*n3, v + 1/8*o1 + 3/8*o3)
  l4 = dt * g(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3, u + 1/8*n1 + 3/8*n3, v + 1/8*o1 + 3/8*o3)
  m4 = dt * h(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3, u + 1/8*n1 + 3/8*n3, v + 1/8*o1 + 3/8*o3)
  n4 = dt * i(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3, u + 1/8*n1 + 3/8*n3, v + 1/8*o1 + 3/8*o3)
  o4 = dt * j(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3, u + 1/8*n1 + 3/8*n3, v + 1/8*o1 + 3/8*o3)
  
  k5 = dt * f(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4, u + 1/2*n1 - 3/2*n3 + 2*n4, v + 1/2*o1 - 3/2*o3 + 2*o4)
  l5 = dt * g(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4, u + 1/2*n1 - 3/2*n3 + 2*n4, v + 1/2*o1 - 3/2*o3 + 2*o4)
  m5 = dt * h(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4, u + 1/2*n1 - 3/2*n3 + 2*n4, v + 1/2*o1 - 3/2*o3 + 2*o4)
  n5 = dt * i(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4, u + 1/2*n1 - 3/2*n3 + 2*n4, v + 1/2*o1 - 3/2*o3 + 2*o4)
  o5 = dt * j(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4, u + 1/2*n1 - 3/2*n3 + 2*n4, v + 1/2*o1 - 3/2*o3 + 2*o4)

  x1 = x + 1/2*k1 - 3/2*k3 + 2*k4
  x2 = x + 1/6*k1 + 2/3*k4 + 1/6*k5
  
  y1 = y + 1/2*l1 - 3/2*l3 + 2*l4
  y2 = y + 1/6*l1 + 2/3*l4 + 1/6*l5

  z1 = z + 1/2*m1 - 3/2*m3 + 2*m4
  z2 = z + 1/6*m1 + 2/3*m4 + 1/6*m5

  u1 = u + 1/2*n1 - 3/2*n3 + 2*n4
  u2 = u + 1/6*n1 + 2/3*n4 + 1/6*n5

  v1 = v + 1/2*o1 - 3/2*o3 + 2*o4
  v2 = v + 1/6*o1 + 2/3*o4 + 1/6*o5
  
  return (x1,y1,z1,u1,v1,x2,y2,z2,u2,v2)



def rkmIter6d(x,y,z,u,v,w,dt,f,g,h,i,j,k):
  k1 = dt * f(x,y,z,u,v,w)
  l1 = dt * g(x,y,z,u,v,w)
  m1 = dt * h(x,y,z,u,v,w)
  n1 = dt * i(x,y,z,u,v,w)
  o1 = dt * j(x,y,z,u,v,w)
  p1 = dt * k(x,y,z,u,v,w)


  k2 = dt * f(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1, u + 1/3*n1, v + 1/3*o1, w + 1/3*p1)
  l2 = dt * g(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1, u + 1/3*n1, v + 1/3*o1, w + 1/3*p1)
  m2 = dt * h(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1, u + 1/3*n1, v + 1/3*o1, w + 1/3*p1)
  n2 = dt * i(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1, u + 1/3*n1, v + 1/3*o1, w + 1/3*p1)
  o2 = dt * j(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1, u + 1/3*n1, v + 1/3*o1, w + 1/3*p1)
  p2 = dt * k(x + 1/3*k1, y + 1/3*l1, z + 1/3*m1, u + 1/3*n1, v + 1/3*o1, w + 1/3*p1)

  k3 = dt * f(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2, u + 1/6*n1 + 1/6*n2, v + 1/6*o1 + 1/6*o2, w + 1/6*p1 + 1/6*p2)
  l3 = dt * g(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2, u + 1/6*n1 + 1/6*n2, v + 1/6*o1 + 1/6*o2, w + 1/6*p1 + 1/6*p2)
  m3 = dt * h(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2, u + 1/6*n1 + 1/6*n2, v + 1/6*o1 + 1/6*o2, w + 1/6*p1 + 1/6*p2)
  n3 = dt * i(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2, u + 1/6*n1 + 1/6*n2, v + 1/6*o1 + 1/6*o2, w + 1/6*p1 + 1/6*p2)
  o3 = dt * j(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2, u + 1/6*n1 + 1/6*n2, v + 1/6*o1 + 1/6*o2, w + 1/6*p1 + 1/6*p2)
  p3 = dt * k(x + 1/6*k1 + 1/6*k2, y + 1/6*l1 + 1/6*l2, z + 1/6*m1 + 1/6*m2, u + 1/6*n1 + 1/6*n2, v + 1/6*o1 + 1/6*o2, w + 1/6*p1 + 1/6*p2)

  k4 = dt * f(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3, u + 1/8*n1 + 3/8*n3, v + 1/8*o1 + 3/8*o3, w + 1/8*p1 + 3/8*p3)
  l4 = dt * g(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3, u + 1/8*n1 + 3/8*n3, v + 1/8*o1 + 3/8*o3, w + 1/8*p1 + 3/8*p3)
  m4 = dt * h(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3, u + 1/8*n1 + 3/8*n3, v + 1/8*o1 + 3/8*o3, w + 1/8*p1 + 3/8*p3)
  n4 = dt * i(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3, u + 1/8*n1 + 3/8*n3, v + 1/8*o1 + 3/8*o3, w + 1/8*p1 + 3/8*p3)
  o4 = dt * j(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3, u + 1/8*n1 + 3/8*n3, v + 1/8*o1 + 3/8*o3, w + 1/8*p1 + 3/8*p3)
  p4 = dt * k(x + 1/8*k1 + 3/8*k3, y + 1/8*l1 + 3/8*l3, z + 1/8*m1 + 3/8*m3, u + 1/8*n1 + 3/8*n3, v + 1/8*o1 + 3/8*o3, w + 1/8*p1 + 3/8*p3)
  
  k5 = dt * f(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4, u + 1/2*n1 - 3/2*n3 + 2*n4, v + 1/2*o1 - 3/2*o3 + 2*o4, w + 1/2*p1 - 3/2*p3 + 2*p4)
  l5 = dt * g(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4, u + 1/2*n1 - 3/2*n3 + 2*n4, v + 1/2*o1 - 3/2*o3 + 2*o4, w + 1/2*p1 - 3/2*p3 + 2*p4)
  m5 = dt * h(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4, u + 1/2*n1 - 3/2*n3 + 2*n4, v + 1/2*o1 - 3/2*o3 + 2*o4, w + 1/2*p1 - 3/2*p3 + 2*p4)
  n5 = dt * i(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4, u + 1/2*n1 - 3/2*n3 + 2*n4, v + 1/2*o1 - 3/2*o3 + 2*o4, w + 1/2*p1 - 3/2*p3 + 2*p4)
  o5 = dt * j(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4, u + 1/2*n1 - 3/2*n3 + 2*n4, v + 1/2*o1 - 3/2*o3 + 2*o4, w + 1/2*p1 - 3/2*p3 + 2*p4)
  p5 = dt * k(x + 1/2*k1 - 3/2*k3 + 2*k4, y + 1/2*l1 - 3/2*l3 + 2*l4, z + 1/2*m1 - 3/2*m3 + 2*m4, u + 1/2*n1 - 3/2*n3 + 2*n4, v + 1/2*o1 - 3/2*o3 + 2*o4, w + 1/2*p1 - 3/2*p3 + 2*p4)

  x1 = x + 1/2*k1 - 3/2*k3 + 2*k4
  x2 = x + 1/6*k1 + 2/3*k4 + 1/6*k5
  
  y1 = y + 1/2*l1 - 3/2*l3 + 2*l4
  y2 = y + 1/6*l1 + 2/3*l4 + 1/6*l5

  z1 = z + 1/2*m1 - 3/2*m3 + 2*m4
  z2 = z + 1/6*m1 + 2/3*m4 + 1/6*m5

  u1 = u + 1/2*n1 - 3/2*n3 + 2*n4
  u2 = u + 1/6*n1 + 2/3*n4 + 1/6*n5

  v1 = v + 1/2*o1 - 3/2*o3 + 2*o4
  v2 = v + 1/6*o1 + 2/3*o4 + 1/6*o5

  w1 = w + 1/2*p1 - 3/2*p3 + 2*p4
  w2 = w + 1/6*p1 + 2/3*p4 + 1/6*p5

  
  return (x1,y1,z1,u1,v1,w1,x2,y2,z2,u2,v2,w2)

