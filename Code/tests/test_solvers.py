import sys
sys.path.insert(0, '/Users/bogdan/CDT-code/miniproject2/Code/')
import unittest
from solver_vanderpol import vanderpol_solve
import numpy as np

class VanDerPolSolverTest(unittest.TestCase):
  def test_harmonic(self):
    ic = [1,0]
    params = [0]
    dt = 0.0001
    tol = 0.0001

    x2pi = vanderpol_solve(ic, 2*np.pi, params, dt)
    x1 = vanderpol_solve(ic, 1, params, dt)    

    self.assertTrue(abs(x2pi[0] - 1) < tol)
    self.assertTrue(abs(x2pi[1] - 0) < tol)
    self.assertTrue(abs(x1[0] - 0.54026023) < tol)
    self.assertTrue(abs(x1[1] + 0.84147099) < tol)

  def test_mu4(self):
    ic = [1,-0.68]
    params = [4]
    dt = 0.0001
    tol = 0.01

    T = 10.200 

    xT = vanderpol_solve(ic, T, params, dt)
    x1 = vanderpol_solve(ic, 1, params, dt)    

    self.assertTrue(abs(xT[0] - 1) < tol)
    self.assertTrue(abs(xT[1] + 0.68 ) < tol)
    print(x1)
    self.assertTrue(abs(x1[0] + 1.82332735) < 0.00001)
    self.assertTrue(abs(x1[1] + 2.63439407)  < 0.00001)    

if __name__ == '__main__':
  unittest.main()
