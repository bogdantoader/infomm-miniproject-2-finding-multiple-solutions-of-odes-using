import numpy as np
import matplotlib.pyplot as plt
import csv
from vanderpol_exact_period import vanderpol_exact_period

filename = "../Solutions/solutions_vanderpol.txt"

mu = []
T_num = []
T_formula = []
with open(filename) as csvfile:
  rdr = csv.reader(csvfile)
  for row in rdr:
    mu_i = float(row[0])
    T_i = float(row[3])
    if T_i > 6 and mu_i > 0: # to ignore stationary solutions and mu=0 
      mu.append(mu_i)
      T_num.append(T_i)
      T_formula.append(vanderpol_exact_period(mu_i))

plt.rc('text', usetex=True)
plt.figure(1)
plt.plot(mu, T_num, 'b', linewidth=2, label='Numerical solution')
plt.plot(mu, T_formula, 'r', linewidth=2, label='Asymptotic formula')
plt.xlabel('\(\mu\)', fontsize=30)
plt.ylabel('T', fontsize=30)
plt.legend(fontsize=20)
plt.show()


    
