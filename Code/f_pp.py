import numpy as np

# for the 2d prey predator model

def fx_pp(u1, u2, p1, p2, p3, p4):
  return p2 * u1 * (1 - u1) - u1 * u2 - p1 * (1 - np.exp(-p3 * u1))
  
def fy_pp(u1, u2, p1, p2, p3, p4):
  return  -u2 + p4 * u1 * u2
