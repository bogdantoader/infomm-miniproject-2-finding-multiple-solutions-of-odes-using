import numpy as np
import matplotlib.pyplot as plt
from solver_mc import *
#from solver_mc import mc_solve_rkm as mc_solve
from augmented_F import get_F_mc
from f_mc import fa_mc


dt = 0.00001
#(r,sigma, zeta, q, pi) = (5.28354999999999, 10, 0.4, 5.0, 2.0)
(r, sigma, zeta, q, pi)  = (4.509999999999974, 1, 0.4, 5.0, 2.0)




#x0 = np.array([0.16398612419453515, -0.080518627861704314, 0.0083732517784211938, -0.26820069707539546, 0.18368326956443967, 9.95])
#x0 = np.array([-1.4504616400509383, -0.46731214293526246, 0.67781833725851692, -0.25628353601918302, 0.92932359493229266,0.005])
#x0 = np.array([1.4504616400509383, 0.46731214293526246, 0.67781833725851692, 0.25628353601918302, 0.92932359493229266, 0.005])


#x0 = np.array([1.38568972,  0.4745292 ,  0.65755021,  0.26646136,  0.92308201,100])


#x0 = np.array([-1.4635805099027985,-0.46580167975548675,0.68173825997008952,-0.25430709566850845,0.93049727187596354,0.0050000029841309503])

#x0 = np.array([3.17909018,   0.96065778,   0.3925501 ,   0.66778605,
#         0.42178766,  16.48659644])
#x0 = np.array([1+0.67455452279589179,0.42415513325017595,0.2150608685391765,0.54514519263358319,0.39648032121911292,19.91859588374761*3])

# sol for 4.509999999999974
#x0 = np.array([0.60663975241844537,0.38342245128076891,0.16800120449123426,0.4492551023397881,0.24939649768157113,10.727827935511201])

x0 = np.array([10*0.60663975241844537,10*0.38342245128076891,0.16800120449123426,0.4492551023397881,0.24939649768157113,10.727827935511201])

T = x0[-1]

if T < 0:
  dt = -dt


(t,a,b,c,d,e) = mc_solve_rkm_vector(x0[0:5],T,[r,sigma,zeta,q,pi],dt)
print(len(t))
print("x(T) = " + repr([a[-1],b[-1],c[-1],d[-1],e[-1]]))

plt.rc('text', usetex=True)


plt.figure(1)
plt.plot(a,b)
plt.xlabel('a', fontsize=20)
plt.ylabel('b', fontsize=20)

plt.figure(2)
plt.plot(t,a, linewidth=2)
plt.xlabel('\(t\)', fontsize=30)
plt.ylabel('\(a\)', fontsize=30)

plt.figure(3)
plt.plot(t,e)
plt.xlabel('t', fontsize=20)
plt.ylabel('e', fontsize=20)




adot = np.zeros(len(a))
for i in range(len(a)):
  adot[i] = fa_mc(a[i],b[i],c[i],d[i],e[i],sigma,r,zeta,q,pi)

plt.figure(4)
plt.plot(a, adot, linewidth=2)
plt.xlabel('\(a\)', fontsize=30)
plt.ylabel('\(\dot{a}\)', fontsize=30)

plt.show()
