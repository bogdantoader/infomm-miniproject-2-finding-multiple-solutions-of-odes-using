import numpy as np
from f_mc import fa_mc
from f_lorenz import fx_lorenz
from f_pp import fx_pp


def get_F_mc(solver, params, dt):

  def F(x):
    n = len(x)
    x0 = x[0:n-1]
    T = x[n-1]
    xT = solver(x0, T, params, dt)
    result = np.zeros(n)
    result[0:n-1] = x0 - xT

    # phase condition f_a(0) = 0
    result[n-1] = fa_mc(x0[0],x0[1],x0[2],x0[3],x0[4],params[1],params[0],params[2],params[3],params[4])


    return result

  return F

def get_F_vanderpol(solver, params, dt):

  def F(x):
    n = len(x)
    x0 = x[0:n-1]
    T = x[n-1]
    xT = solver(x0, T, params, dt)
    result = np.zeros(n)
    result[0:n-1] = x0 - xT

    # phase condition x(0) = 1
    #result[n-1] = x[0] - 1

    # phase condition y(0) = 0 (i.e. dx/dt(0) = 0 )
    result[n-1] = x[1]

    return result

  return F

def get_F_lorenz(solver, params, dt):

  def F(x):
    n = len(x)
    x0 = x[0:n-1]
    T = x[n-1]
    xT = solver(x0, T, params, dt)
    result = np.zeros(n)
    result[0:n-1] = x0 - xT

    # phase condition f_a(0) = 0
    result[n-1] = fx_lorenz(x0[0],x0[1],x0[2],params[1],params[0],params[2])


    return result

  return F


def get_F_pp(solver, params, dt):

  def F(x):
    n = len(x)
    x0 = x[0:n-1]
    T = x[n-1]
    xT = solver(x0, T, params, dt)
    result = np.zeros(n)
    result[0:n-1] = x0 - xT

    # phase condition f_a(0) = 0
    result[n-1] = fx_pp(x0[0],x0[1],params[0],params[1],params[2],params[3])

    return result

  return F
