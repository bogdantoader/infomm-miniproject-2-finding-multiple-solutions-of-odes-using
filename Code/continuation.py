import numpy as np
from deflation import deflation
from colours import *
import collections

# m0 and mF are lists of parameters, identical except the first one.
# continuation is done based on the first parameter, between m0[0] and mF[0]
# ics is a list of np arrays - it contains all the known solutions of the
# system for the m0 parameter value
def continuation(m0, mF, dm, ics, solver, get_F, calculate_jacobian, tol, eps, dt,iters):
  all_sols = collections.OrderedDict([(m0[0], ics)])
  m = m0.copy()
  #while m[0] > mF[0]:
  while m[0] < mF[0]:
    old_param = m[0]
    m[0] = m[0] + dm
    printGreen("m = " + repr(m[0]))
    # residual for new parameter value
    F = get_F(solver, m, dt)
  
    current_sols = []
    for ic in all_sols[old_param]:
      sols = deflation(F, ic , calculate_jacobian, tol, iters)
      current_sols += sols
  
    # Remove duplicate (non-zero) solutions
    if current_sols:
      current_unique_sols = [current_sols[0]]
    else:
      current_unique_sols = []
      
    for s in current_sols:
      idx = find(current_unique_sols, s, tol)
      if idx == -1:
        current_unique_sols.append(s)
      else:
        if abs(s[-1]) < abs(current_unique_sols[idx][-1]): # we want the solution with the smallest period
          current_unique_sols.pop(idx)
          current_unique_sols.append(s)

    # Now remove all duplicate zero solutions
    #zero_sols = list(filter(lambda x: abs(np.linalg.norm(x[0:len(x)-1],2)) < tol , current_unique_sols))
    #for s in zero_sols:
    #  current_unique_sols.remove(s)
    #if zero_sols:
    #  current_unique_sols.append(zero_sols[0])
   
    # if it has a very large negative exponent, set it to tol 
    # to avoid underflow
    for s in current_unique_sols:
      for (idx,n) in enumerate(s):
        if abs(n) < tol:
          s[idx] = tol


    #all_sols[m[0]] =  list(map(np.asarray, current_unique_sols)) 
    all_sols[m[0]] =  current_unique_sols
    printBlue("Solutions for m = " + repr(m[0]) + ": ")
    printBlue(repr(all_sols[m[0]]))
  
  return all_sols

# The solutions are assumed to be numpy arrays
# It only compares the starting points, not the period (the last element)
# i.e. two different solutions with the same starting point but different
# periods are considered to be the same solution
def find(mylist, sol, tol):
  n = len(sol)
  for idx, val in enumerate(mylist):
    if max(abs(val[0:n-1] - sol[0:n-1])) < tol:
      return idx
  return -1


