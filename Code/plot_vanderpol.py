import numpy as np
import matplotlib.pyplot as plt
from solver_vanderpol import *
from augmented_F import get_F_vanderpol

dt = 0.00001

# periodic solution for mu=4
#x0 = 1 
#y0 = -0.68

# periodic solution for mu=5
#x0 = 1.0
#y0 = -0.63108233172742845
#T = 11.60702755

# periodic solution for mu=6
#x0 = 1.0
#y0 = -0.59025546
#T = 13.05468271

# periodic sol for mu=6.5
#x0 = 1.0
#y0 = -0.57328626183024323
##T = 13.789403611556299

# sol for mu=6.9
#x0 = 1.0
#y0 = -0.56098413207836539
#T = 14.381579258409966

# sol for mu=7
#x0 = 1.0
#y0 = -0.55806575459787033
#T = 14.530094397873762

# sol for mu=8
#x0 = 1.0
#y0 = -0.53177512627743617
#T = 16.025702001224069


mu = 4
x0 = np.array([1, -0.6855, 10.2])

T = x0[-1]

if T < 0:
  dt = -dt

(t,x,y) = vanderpol_solve_rkm_vector(x0[0:2], T, [mu], dt)
print(len(t))
print("x(T) = " + repr([x[-1], y[-1]]))


plt.figure(1)
plt.plot(x, y, linewidth=2)
#plt.title('phase plane')
plt.xlabel('x', fontsize=20)
plt.ylabel('y', fontsize=20)


plt.figure(2)
plt.plot(t, x, linewidth=2)
plt.xlabel('t', fontsize=20)
plt.ylabel('x', fontsize=20)

plt.figure(3)
plt.plot(t, y, linewidth=2)
plt.xlabel('t', fontsize=20)
plt.ylabel('y', fontsize=20)


plt.show()


