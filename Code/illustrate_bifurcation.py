import numpy as np
import matplotlib.pyplot as plt
from solver_mc import mc_solve_rkm_vector_6d

dt = 0.00001

# good-ish plot
(sigma, zeta, q, pi, C) = (1, 0.4, 5, 2.0, 0.017)
#r0 = 2.7
#rMax = 5.32
#rMax = 6
r0 = 4
rMax = 5.27
T = 1/C*(rMax-r0)
print(T)
#x0 = np.array([0.05, 0.05, 0.05, 0.05, 0.05, r0])
x0 = np.array([0.10965183433630937, -0.090150452155441652, 0.0094381364570997692, -0.30844285279126815, 0.12878270956076721, 4.0000002266666668])

#(sigma, zeta, q, pi, C) = (1, 0.4, 20, 2.0, 0.1)
#r0 = 7
#rMax = 10
#T = 1/C*(rMax-r0)
#x0 = np.array([0.5, 0.5, 0.5, 0.5, 0.5, r0])


(t,a,b,c,d,e,r) = mc_solve_rkm_vector_6d(x0,T,[sigma,zeta,q,pi,C],dt)
print("x(T) = " + repr([a[-1],b[-1],c[-1],d[-1],e[-1], r[-1]]))

plt.rc('text', usetex=True)
  
plt.figure(1)
plt.plot(t, r, linewidth=2)
plt.xlabel('t', fontsize=20)
plt.ylabel('r', fontsize=20)

plt.figure(2)
plt.plot(t, a, linewidth=2)
plt.xlabel('\(t\)', fontsize=30)
plt.ylabel('\(a\)', fontsize=30)


plt.show()
