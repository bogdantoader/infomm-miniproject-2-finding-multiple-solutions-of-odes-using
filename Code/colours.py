HEADER = '\033[95m'
OKBLUE = '\033[94m'
OKGREEN = '\033[92m'
WARNING = '\033[93m'
FAIL = '\033[91m'
ENDC = '\033[0m'
BOLD = '\033[1m'
UNDERLINE = '\033[4m'

def printRed(text):
  print(FAIL + text + ENDC)

def printGreen(text):
  #print(OKGREEN + text + ENDC)
  print(HEADER + text + ENDC)

def printBlue(text):
  print(OKGREEN + text + ENDC)

