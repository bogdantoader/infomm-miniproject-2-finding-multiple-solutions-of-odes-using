import numpy as np

from jacobian import calculate_jacobian
from deflation import deflation
from augmented_F import get_F_vanderpol
from solver_vanderpol import vanderpol_solve
from continuation import continuation
from create_csv import create_csv

np.seterr(all='raise')

# general parameters
iters = 12
tol = 1e-10
dt = 0.00001
eps = 0.00001


# continuation parameters

# ic for mu =0
#ic = [np.array([1, 0, 2*np.pi])]

# ic for mu = 4
#ic = [np.array([2.0229625009685983,0.0,10.203523691000093]),
#np.array([3.777107160298377e-12,-5.057516872626712e-28,1.0951374015045279])]

# ic for mu=10.1
#ic = [np.array([2.0141775021443551,0.0,19.231558220310973]),
#np.array([3.3935026544172867e-14,-1.1710839199259674e-30,1.3576650067421205])]

# ic for mu = 20.0
#ic = [np.array([2.0077899708652445,0.0,34.682323311658344]),
#np.array([3.7731840043596938e-14,1.7096349838789412e-34,0.75375055003542046])]

# ic for mu = 26.65
ic = [np.array([2.005842448167062,0.0,45.215495374797477]),
np.array([3.7732818927292014e-14,1.3023173346926026e-35,0.7080913273385685])]


m0 = [26.65]
dm = 0.01
mF = [30]


# some functions
def jacobian(F, x):
  return calculate_jacobian(F, x, eps)

# and run
sols = continuation(m0, mF, dm, ic, vanderpol_solve, get_F_vanderpol, jacobian, tol, eps, dt, iters)

output = create_csv(sols)
print(output)
#f = open('vanderpol_sols26-30.txt', 'w')
#f.write(output)
#f.close()
