import numpy as np
from settings import *
from myexceptions import * 
from colours import *


def newtons_method(F, x0, calculate_jacobian, tol, max_iters):
  r = F(x0)
  resNorm = np.linalg.norm(r,2)

  iters = 0
  while resNorm > tol:
    print("iters = " + repr(iters))
    if verbose:
      print("  x0 = " + repr(x0))
                    
    J = calculate_jacobian(F, x0)
    if verbose:
      print("  Calling solve")
      print("  J = " + repr(J))
      print("  r = " + repr(r))
    dx = np.linalg.solve(J, -r)
    x0 = x0 + dx
    print("  Solve done.")
    print("  dx = " + repr(dx))
    print("  New sol x0 = " + repr(x0))
 
    # If T is negative or too large, do a line search (on a course grid)
    # and minimise
    # for mc perMax = 300 and perMin = 0
    # for vanderpol perMax = 3000, perMin = -10
    perMin = -1
    perMax = 3000
    if x0[-1] < perMin or x0[-1] > perMax:
      #def f(x):
      #  y0 = x0.copy()
      #  y0[-1] = x
      #  return np.linalg.norm(F(y0),2)

      #gridvals = list(map(f, range(1,300)))
      #x0[-1] = 1 + np.argmin(gridvals)
      #print("  new T = " + repr(x0[-1]))
      #resNorm = min(gridvals)

      # no line search for mc. only set to 0.001
      x0[-1] = 0.001

      print("  new T = " + repr(x0[-1]))
      resNorm = np.linalg.norm(F(x0), 2)
    else:
      r = np.asarray(F(x0))
      resNorm = np.linalg.norm(r, 2)
      printBlue("  residual = " + repr(r))

    iters = iters + 1
  
    printBlue("  norm of residual = " + repr(resNorm))
    if iters == max_iters:
      raise MaxIterationsException
    if resNorm > 10000:
      raise ResidualTooLargeException

  return x0 

