import numpy as np
#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import csv
from solver_pp import pp_solve_rkm_vector


filename = "pp_sols2.txt"
dt = 0.00001

p2 = 3
p4 = 3
p3 = 5

plt.rc('text', usetex=True)
plt.figure(1)

with open(filename) as csvfile:
  rdr = csv.reader(csvfile)
  for row in rdr:
    p1 = float(row[0])
    
    x = np.array([ float(row[1]),float(row[2]),float(row[3])])
    #print(repr(r) + ", " + repr(x))

    params = [p1, p2, p3, p4]
    (t,a,b) = pp_solve_rkm_vector(x[0:2],x[2],params,dt)
    #if x[0] > 0:
    #  p = max(a)
    #else:
    #  p = min(a)
    p = max(a)
    
    if x[-1] > 1 and abs(x[0]) > 1e-8:
      plt.plot(p1, p, 'r.')
    else:
      plt.plot(p1, p, 'b.')


plt.xlabel('\(p_1\)', fontsize=30)
plt.ylabel('\(y_{max}\)', fontsize=30)
#plt.savefig('bifurcation_q5')
plt.show()



