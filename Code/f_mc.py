

def fa_mc(a,b,c,d,e,sigma,r,zeta,q,pi):
  return sigma * (-a+r*b + zeta*q*d*((pi-3)*e - 1))

def fb_mc(a,b,c,d,e,sigma,r,zeta,q,pi):
  return -b + a*(1 - c)

def fc_mc(a,b,c,d,e,sigma,r,zeta,q,pi):
  return pi * (-c + a*b)

def fd_mc(a,b,c,d,e,sigma,r,zeta,q,pi):
  return -zeta*d + a * (1 - e)

def fe_mc(a,b,c,d,e,sigma,r,zeta,q,pi):
  return -(4 - pi)*zeta*e + pi*a*d
