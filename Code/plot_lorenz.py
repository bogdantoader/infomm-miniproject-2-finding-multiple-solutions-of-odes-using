import numpy as np
import matplotlib.pyplot as plt
from solver_lorenz import *
from augmented_F import get_F_lorenz



dt = 0.00001

# a sol
#x0 = 1
#y0 = 3.5558322664684767
#z0 = 29.202375572241596
##T = -1.678431871198637

# another (?) sol
#x0 = 1.0
#y0 = 1.687986993240844
#z0 = 18.062273542119261
#T = 1.6784779396595972
#N = 6700
#N = 2*N

beta = 8.0/3
sigma = 10.0

rho  = 34.5
#x0 = np.array([-1.15653515,  -1.15653515,  21.89026657,   5.33716134])
x0 = np.array([1-1.15653515,  -1.15653515,  21.89026657,   30*5.33716134])

#rho = 99.06
#x0 = np.array([16.17075549667803,16.17075549667803,98.060000000004209,10000*0.0010277924853391972])

T = x0[-1]

if T < 0:
  dt = -dt


# extra plot for the lay report cover
x1 = np.array([-1.15653515,  -1.15653515,  21.89026657,   5.33716134])
(t1,x1,y1,z1) = lorenz_solve_rkm_vector(x1[0:3], x1[-1], [rho,sigma,beta], dt)


(t,x,y,z) = lorenz_solve_rkm_vector(x0[0:3], T, [rho,sigma,beta], dt)
print(len(t))
print("x(T) = " + repr([x[-1], y[-1], z[-1]]))

plt.figure(1)
#plt.subplot(212)
#plt.plot(t,x)
plt.plot(x, y)
plt.xlabel('x', fontsize=20)
plt.ylabel('y', fontsize=20)

plt.figure(2)
plt.plot(x,z)
plt.xlabel('x', fontsize=20)
plt.ylabel('z', fontsize=20)
plt.plot(x1,z1,'r', linewidth=2)

plt.figure(3)
plt.plot(y, z)
plt.xlabel('y', fontsize=20)
plt.ylabel('z', fontsize=20)

plt.show()



