import numpy as np
#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import csv
from solver_vanderpol import vanderpol_solve_rkm_vector


filename = "../Solutions/solutions_vanderpol.txt"
dt = 0.00001

plt.rc('text', usetex=True)
plt.figure(1)

with open(filename) as csvfile:
  rdr = csv.reader(csvfile)
  for row in rdr:
    mu = float(row[0])
    
    x = np.array([ float(row[1]),float(row[2]),float(row[3])])
    #print(repr(r) + ", " + repr(x))

    params = [mu]
    (t,a,b) = vanderpol_solve_rkm_vector(x[0:2],x[2],params,dt)
    if x[0] > 0:
      p = max(b)
    else:
      p = min(b)

    
    if x[-1] > 1 and abs(x[0]) > 1e-8:
      plt.plot(mu, p, 'r.')
    else:
      plt.plot(mu, p, 'b.')


plt.xlabel('\(\mu\)', fontsize=30)
plt.ylabel('\(y_{max}\)', fontsize=30)
#plt.savefig('bifurcation_q5')
plt.show()



