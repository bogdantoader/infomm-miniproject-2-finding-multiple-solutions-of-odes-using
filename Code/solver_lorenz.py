import numpy as np

from f_lorenz import fx_lorenz as fx
from f_lorenz import fy_lorenz as fy
from f_lorenz import fz_lorenz as fz
from rk import rk3d
from rk import rkmIter3d
from myexceptions import *
from settings import *


# params = [sigma, rho, beta]
def lorenz_solve(ic, T, params, dt):
  if T < 0:
    dt = -dt

  #return lorenz_solve_euler_expl(ic, T, params, dt)
  #return lorenz_solve_rk(ic, T, params, dt)
  return lorenz_solve_rkm(ic, T, params, dt)

def lorenz_solve_euler_expl(ic, T, params, dt):
  [x0, y0, z0] = ic
  [sigma, rho, beta] = params
  x = x0
  y = y0
  z = z0
  t = 0
  while t < T:
    x0 = x
    y0 = y
    z0 = z
    x = x0 + dt * fx(x0, y0, z0, sigma, rho, beta)
    y = y0 + dt * fy(x0, y0, z0, sigma, rho, beta)
    z = z0 + dt * fz(x0, y0, z0, sigma, rho, beta)
    t = t + dt
  return [x,y,z]

def lorenz_solve_rk(ic, T, params, dt):
  [x0, y0, z0] = ic
  [sigma, rho, beta] = params
  x = x0
  y = y0
  z = z0
  t = 0

  def f(t, x, y, z):
    return fx(x,y,z,sigma,rho,beta)

  def g(t, x, y, z):
    return fy(x,y,z,sigma,rho,beta)

  def h(t, x, y, z):
    return fz(x,y,z,sigma,rho,beta)

  while t < T:
    (xn,yn,zn) = rk3d(t, x, y, z, dt, f, g, h)
    x = xn
    y = yn
    z = zn
    t = t + dt
  return [x,y,z]


rkm_epsilon = 1e-11
timestep_tol = 1e-8
#  [rho,sigma,beta] = params
def lorenz_solve_rkm(ic, T, params, dt):
  epsilon = rkm_epsilon
  [a0, b0, c0] = ic
  [rho,sigma,beta] = params
  a = a0
  b = b0
  c = c0
  t = 0

  def f(a,b,c):
    return fx(a,b,c,sigma,rho,beta)
  
  def g(a,b,c):
    return fy(a,b,c,sigma,rho,beta)

  def h(a,b,c):
    return fz(a,b,c,sigma,rho,beta)

  # We need to prevent the situation where it only doubles and halves the dt 
  # infinitely many times and does not change t.
  # This is what idx1 and idx2 do
  idx1 = 0
  idx2 = 0
  while abs(t) < abs(T):
    (an1,bn1,cn1,an2,bn2,cn2) = rkmIter3d(a,b,c,dt,f,g,h)
    R = 0.2 * np.linalg.norm([an1-an2, bn1-bn2, cn1-cn2])

    if idx1 > 10 and abs(idx1 - idx2) <= 1:
      R = epsilon

    if R > epsilon:
      dt = dt / 2
      idx1 +=1
    elif R > epsilon / 64:
      # In case the step size is too large, reduce it so that we calculate the 
      # the state at T
      if (abs(t + dt) > abs(T)):
        dt = T - t
        (an1,bn1,cn1,an2,bn2,cn2) = rkmIter3d(a,b,c,dt,f,g,h)
      
      a = an2
      b = bn2
      c = cn2
      t += dt
      idx1 = 0
      idx2 = 0
    else: # if R leq epsilon/64
      dt = dt * 2
      idx2 += 1

    #if abs(dt) < timestep_tol:
    #  raise TimeStepTooSmallException

  return [a,b,c]


# This is the same as vadernpol_solve_rkm except 
# that it returns all the values in vectors,
# rather than only the value at T.
# Needed for plotting
def lorenz_solve_rkm_vector(ic, T, params, dt):
  epsilon = rkm_epsilon
  [a0, b0, c0] = ic
  [rho,sigma,beta] = params
  a = [a0]
  b = [b0]
  c = [c0]
  t = [0]

  def f(a,b,c):
    return fx(a,b,c,sigma,rho,beta)
  
  def g(a,b,c):
    return fy(a,b,c,sigma,rho,beta)

  def h(a,b,c):
    return fz(a,b,c,sigma,rho,beta)

  
  # We need to prevent the situation where it only doubles and halves the dt 
  # infinitely many times and does not change t.
  # This is what idx1 and idx2 do
  idx1 = 0
  idx2 = 0
  while abs(t[-1]) < abs(T):
    #print()
    if verbose:
      print("dt = " + repr(dt))
      print(" t  = " + repr(t[-1]))
    #print(" idx= " + repr(idx))
    (an1,bn1,cn1,an2,bn2,cn2) = rkmIter3d(a[-1],b[-1],c[-1],dt,f,g,h)
    R = 0.2 * np.linalg.norm([an1-an2, bn1-bn2, cn1-cn2])
    #print("R  = " + repr(R))
    #print("eps= " + repr(epsilon))

    if idx1 > 10 and abs(idx1 - idx2) <= 1:
      R = epsilon

    if R > epsilon:
      dt = dt / 2
      idx1 +=1
    if R <= epsilon and R > epsilon / 64:
      if (abs(t[-1] + dt) > abs(T)):
        dt = T - t[-1]
        (an1,bn1,cn1,an2,bn2,cn2) = rkmIter3d(a[-1],b[-1],c[-1],dt,f,g,h)
 
      a.append(an2)
      b.append(bn2)
      c.append(cn2)
      t.append(t[-1] + dt)
      idx1 = 0
      idx2 = 0
    if R <= epsilon/64:
      dt = dt * 2
      idx2 += 1
  
  return (t,a,b,c)


    
