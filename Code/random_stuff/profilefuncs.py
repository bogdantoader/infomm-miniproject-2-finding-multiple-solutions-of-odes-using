import sys
sys.path.insert(0, '/Users/bogdan/CDT-code/miniproject2/Code/')
import numpy as np
import cProfile
from solver_vanderpol import vanderpol_solve


mu = 4
x = 1
y = 6.38690495
T = 12.65337827
dt = 0.0001
eps = 0.0001
x0 = np.array([x,y])

def func():
  vanderpol_solve(x0, T, [mu], dt)
  return 0

cProfile.run('func()')





