import sys
sys.path.insert(0, '/Users/bogdan/CDT-code/miniproject2/Code/')
import numpy as np
import matplotlib.pyplot as plt
from solver_vanderpol import vanderpol_solve

mu = 1
dt = 0.001
N = 80000

t = np.zeros((N,1))

# periodic solution for mu=4
#x0 = 1 
#y0 = -0.68
#x = np.zeros((N,1))
#x[0] = x0;
#y = np.zeros((N,1))
#y[0] = y0;

# out
xo0 = 5
yo0 = 4
xo = np.zeros((N,1))
xo[0] = xo0;
yo = np.zeros((N,1))
yo[0] = yo0;

# in
xi0 = 0.5
yi0 = 0.1
xi = np.zeros((N,1))
xi[0] = xi0;
yi = np.zeros((N,1))
yi[0] = yi0;


for i in range(1,N):
  #(xn, yn) = vanderpol_solve([x[i-1], y[i-1], dt, [mu], dt)
  #x[i] = xn
  #y[i] = yn
  
  (xon, yon) = vanderpol_solve([xo[i-1], yo[i-1]], dt, [mu], dt)
  xo[i] = xon
  yo[i] = yon

  (xin, yin) = vanderpol_solve([xi[i-1], yi[i-1]], dt, [mu], dt)
  xi[i] = xin
  yi[i] = yin

  
  t[i] = t[i-1] + dt

#print(N * dt)
#print(x[0])
#print(x[1])
#print(x[2])
#print(x[3])
#print(x[4])
#
#print(x)
#print(y)
#
#print(x[N-1])
#print(y[N-1])

#plt.figure(1)
#plt.subplot(211)
#plt.plot(x,y, 'b', 

plt.figure(1)
plt.plot(xo, yo, 'r', xi, yi, 'b')
#plt.title('phase plane')
plt.xlabel('x', fontsize=20)
plt.ylabel('y', fontsize=20)


plt.figure(2)
#plt.subplot(212)
#plt.plot(t,x)
plt.plot(t, xo, 'r', t, xi, 'b')
plt.xlabel('t', fontsize=20)
plt.ylabel('x', fontsize=20)

plt.show()


