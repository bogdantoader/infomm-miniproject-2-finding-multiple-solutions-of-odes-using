import numpy as np

#Dear Bogdan,
#
#	My notes from E.J.Hinch (who has since published a book on asymptotic methods) give the period of van der Pol as 
#
#T  \sim \mu(3-2 \ln 2) + 3 \mu^{-1/3} s_0
#
#Where s_0 \approx 2.34.
#
#Wayne


s0 = 2.34

#for mu in np.linspace(0.2, 4.4, 22):
for mu in [4.7, 5.1, 5.3, 5.5, 6.8, 10.1, 11.1, 11.3, 12.7, 14.4, 16.6, 20, 23.1, 26.65, 27.7, 30]:
  T = mu * (3 - 2* np.log(2)) + 3 * mu**(-1.0/3)*s0
  print("mu = " + repr(mu) + ": T = " + repr(T))
