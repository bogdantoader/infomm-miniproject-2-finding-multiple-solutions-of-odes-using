import numpy as np
#import matplotlib
#matplotlib.use('Agg')
import matplotlib.pyplot as plt
import csv

filename = "solutions_q25_raw.txt"
with open(filename) as csvfile:
  rdr = csv.reader(csvfile)
  for row in rdr:
    r = float(row[0])
    p = float(row[1])
    rob = int(row[2])

    if rob == 1:
      plt.plot(r,p,'r.')
    else:
      plt.plot(r,p,'b.')

plt.show()
